

from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db import IntegrityError

from .models import Dog, Walk, WalkType
# Create your views here.

def index(request):
    return render(request, 'dash/index.html')

def journal(request):
    walks = Walk.objects.all()
    return render(request, 'dash/journal.html', {'walks': walks})

def profil(request):
    dogs = Dog.objects.all()
    return render(request, 'dog/profil.html', {'dogs': dogs})


def parametres(request):
    return render(request, 'dash/parametres.html')



def my_login(request):
    return render(request, 'dash/login.html')

def register(request):
    return render(request, 'dash/register.html')

def my_logout(request):
    logout(request)
    return render(request, 'dash/logout.html')

def registered(request):
    name = request.POST['user_name']
    firstname = request.POST['user_firstname']
    pwd = request.POST['user_pwd']
    email = request.POST['user_email']
    username = firstname[0].lower() + "." + name.lower()        
    try:
        user = User.objects.create_user(username, email, pwd)
        user.last_name = name
        user.first_name = firstname
        user.save()
        context = {'user':user}   
        return render(request, 'dash/registered.html', context)            
    except IntegrityError :
        return render(request,'dash/register.html', {'error_message': "le nom d'utilisateur existe déja !"})
    

def welcome(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    context = {'user':user}
    if user is not None:
        login(request, user)
        return render(request, 'dash/welcome.html', context)
    else:
        return render(request, 'dash/error_log.html')


def dog_register(request): 
    # if request.method == 'POST':
    #     form = AddDog(request.POST or None, request.FILES or None)
    #     if form.is_valid():
    #         form.save()
    #     return render(request, 'dog/dog_register.html')
    # else:    
    #     form = AddDog()
    return render(request, 'dog/dog_register.html',)



# dog_name = request.POST['name_dog']






















def ajouter(request):
    return render(request, 'dash/ajouter.html')

def maps(request):
    return render(request, 'dash/maps.html')

def budget(request):
    return render(request, 'dash/budget.html')

def objectif(request):
    return render(request, 'dash/objectif.html')
