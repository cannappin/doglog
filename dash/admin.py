from django.contrib import admin
from .models import Owner, Breed, Dog, Vaccine, Feeding, FoodBrand, FoodType, Photo, Walk, WalkType
# Register your models here.


admin.site.register(Owner)
admin.site.register(Breed)
admin.site.register(Dog)
admin.site.register(Vaccine)
admin.site.register(Feeding)
admin.site.register(FoodBrand)
admin.site.register(FoodType)
admin.site.register(Photo)
admin.site.register(Walk)
admin.site.register(WalkType)

