from django.urls import path

from . import views

app_name = 'dash'

urlpatterns = [
    path('ajouter', views.ajouter, name='ajouter'),
    path('maps', views.maps, name='maps'),
    path('budget', views.budget, name='budget'),
    path('objectif', views.objectif, name='objectif'),
    path('', views.index, name='index'),
    path('journal', views.journal, name='journal'),
    path('profil', views.profil, name='profil'), 
    path('parametres', views.parametres, name='parametres'),    
    path('logout', views.my_logout, name='logout'),
    path('login', views.my_login, name='login'),
    path('register', views.register, name='register'),
    path('registered', views.registered, name='registered'),
    path('welcome', views.welcome, name='welcome'),
    path('dog_register', views.dog_register, name='dog_register'),
    
]
